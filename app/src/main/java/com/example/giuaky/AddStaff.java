package com.example.giuaky;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;
import java.util.List;

public class AddStaff extends AppCompatActivity {

    private LinearLayout linBack;
    private Button btnSave;
    private EditText edtID, edtName;
    private ImageView imgAva;
    private Spinner spnFactory;

    private DatabaseHelper myDatabase;

    private Staff newStaff;
    List<Integer> listFactory = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_staff);

        myDatabase = new DatabaseHelper(this);
        myDatabase.createDB();
        myDatabase.openDB();

        linBack = findViewById(R.id.lin_add_staff_back);
        btnSave = findViewById(R.id.btn_add_staff_save);
        edtID = findViewById(R.id.edt_add_staff_id);
        edtName = findViewById(R.id.edt_add_staff_fullname);
        imgAva = findViewById(R.id.img_add_staff_ava);
        spnFactory = findViewById(R.id.spn_add_staff_factory);

        linBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });

        edtID.setText(myDatabase.getNextStaffID());

        Transformation transformation = new RoundedTransformationBuilder()
                .oval(true)
                .build();
        Picasso.get()
                .load("https://ryl16zv916obj.vcdn.cloud/wp-content/uploads/2020/04/default-profile.png")
                .transform(transformation)
                .into(imgAva);

        Intent backMain = new Intent(this, MainActivity.class);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String fname = edtName.getText().toString().substring(0, edtName.getText().toString().lastIndexOf(' '));
                String lname = edtName.getText().toString().substring(edtName.getText().toString().lastIndexOf(' ')+1, edtName.getText().toString().length());

                newStaff = new Staff(edtID.getText().toString(), fname, lname, listFactory.get(spnFactory.getSelectedItemPosition()), "https://ryl16zv916obj.vcdn.cloud/wp-content/uploads/2020/04/default-profile.png");

                myDatabase.addStaff(newStaff);

                setResult(Activity.RESULT_OK);
                finish();
            }
        });

        listFactory.add(1);
        listFactory.add(2);
        listFactory.add(3);
        listFactory.add(4);

        ArrayAdapter<Integer> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, listFactory);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spnFactory.setAdapter(adapter);


    }
}