package com.example.giuaky;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;

public class Details extends AppCompatActivity {

    private LinearLayout btn_Back;
    private ImageView imgStaffAva;
    private TextView txtStaffName, txtStaffID, txtStaffFactory, txtGoodProducts, txtBadProducts, txtSumTotal;
    private Button btnDetailsReport;
    private RecyclerView recyclerStaffDays;
    private ListView lstViewDetails;

    private Staff staff;

    private DatabaseHelper myDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor("#f6f6f6"));
        }
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        myDatabase = new DatabaseHelper(this);
        myDatabase.createDB();
        myDatabase.openDB();

        Intent intent = getIntent();
        String staffID = intent.getStringExtra("staffID");

        staff = myDatabase.getStaff(staffID);

        btn_Back = findViewById(R.id.lin_details_back);
        imgStaffAva = findViewById(R.id.img_details_ava);
        txtStaffName = findViewById(R.id.txt_details_staff_name);
        txtStaffID = findViewById(R.id.txt_details_staff_id);
        txtStaffFactory = findViewById(R.id.txt_details_staff_factory);
        btnDetailsReport = findViewById(R.id.btn_details_report);
        txtGoodProducts = findViewById(R.id.txt_details_sum_good_product);
        txtBadProducts = findViewById(R.id.txt_details_sum_bad_product);
        txtSumTotal = findViewById(R.id.txt_details_sum_total_price);

        recyclerStaffDays = findViewById(R.id.list_staff_days);
        lstViewDetails = findViewById(R.id.lstv_detail);

        txtStaffName.setText(staff.getfName()+" "+staff.getlName());
        txtStaffID.setText(staff.getId());
        txtStaffFactory.setText("Phân xưởng "+staff.getFactory());

        Transformation transformation = new RoundedTransformationBuilder()
                .oval(true)
                .build();
        Picasso.get()
                .load(staff.getImage())
                .transform(transformation)
                .into(imgStaffAva);

        btn_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        StaffDaysRecyclerViewAdapter staffDaysRecyclerViewAdapter = new StaffDaysRecyclerViewAdapter(this, staff.getDays(), new ItemClickInterface() {
            @Override
            public void onDayClick(String id) {

//                Toast.makeText(Details.this, "Detail: "+staff.getDay(id).getDay(), Toast.LENGTH_SHORT).show();
                DetailListViewAdapter detailListViewAdapter = new DetailListViewAdapter(Details.this, R.layout.details_item, staff.getDay(id).getDetailsList());
                lstViewDetails.setAdapter(detailListViewAdapter);

                txtGoodProducts.setText(staff.getDay(id).getCountGoodProduct()+"");
                txtBadProducts.setText(staff.getDay(id).getCountBadProduct()+"");
                txtSumTotal.setText(staff.getDay(id).getSumPrice()+"");

            }
        });
        recyclerStaffDays.setAdapter(staffDaysRecyclerViewAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerStaffDays.setLayoutManager(linearLayoutManager);

        if(staff.getDays().size()>0){
            Day firstDay = staff.getDays().get(0);
            DetailListViewAdapter detailListViewAdapter = new DetailListViewAdapter(this, R.layout.details_item, firstDay.getDetailsList());
            lstViewDetails.setAdapter(detailListViewAdapter);

            txtGoodProducts.setText(firstDay.getCountGoodProduct()+"");
            txtBadProducts.setText(firstDay.getCountBadProduct()+"");
            txtSumTotal.setText(firstDay.getSumPrice()+"");
        }
        else{
            txtGoodProducts.setText("0");
            txtBadProducts.setText("0");
            txtSumTotal.setText("0");
        }

    }
}